#
# SPDX-FileCopyrightText: 2010-2022 by Gilles Caulier, <caulier dot gilles at gmail dot com>
#
# SPDX-License-Identifier: BSD-3-Clause
#

if(KF${QT_VERSION_MAJOR}Sane_FOUND)
    add_subdirectory(dscanner)
endif()
