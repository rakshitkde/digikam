#!/bin/bash

# SPDX-FileCopyrightText: 2008-2023 by Gilles Caulier, <caulier dot gilles at gmail dot com>
#
# SPDX-License-Identifier: BSD-3-Clause
#
# Arguments : $1 : VCPKG install path (default ./project/mxe/build.win32).
#             $2 : build type : 'debugfull' to hack (default), 'release' for production, relwithdebinfo for packaging.
#             $3 : Cmake extra configure options.

# Halt and catch errors
set -eE
trap 'PREVIOUS_COMMAND=$THIS_COMMAND; THIS_COMMAND=$BASH_COMMAND' DEBUG
trap 'echo "FAILED COMMAND: $PREVIOUS_COMMAND"' ERR

VCPKG_TRIPLET="x64-windows"

INSTALL_DIR=$1

if [[ $INSTALL_DIR == "" ]]; then

    INSTALL_DIR="/e/dk/"

fi

BUILD_TYPE=$2

if [ "$BUILD_TYPE" = "" ]; then
    BUILD_TYPE=RelWithDebInfo
fi

export PATH=$PATH:/c/bison:/c/icoutils/bin:$INSTALL_DIR/$VCPKG_TRIPLET/tools/gperf
echo "PATH=$PATH"

if [ ! -d "build" ]; then
    mkdir build
fi

cd build

cmake .. -DCMAKE_TOOLCHAIN_FILE=/c/vcpkg/scripts/buildsystems/vcpkg.cmake \
                           -DVCPKG_TARGET_TRIPLET=$VCPKG_TRIPLET \
                           -DCMAKE_COLOR_MAKEFILE=ON \
                           -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
                           -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR/$VCPKG_TRIPLET \
                           -DCMAKE_BUILD_TYPE=$BUILD_TYPE \
                           -DINSTALL_ROOT=$INSTALL_DIR/$VCPKG_TRIPLET \
                           -DBUILD_TESTING=OFF \
                           -DBUILD_WITH_QT6=ON \
                           -DDIGIKAMSC_COMPILE_PO=OFF \
                           -DDIGIKAMSC_COMPILE_DIGIKAM=ON \
                           -DENABLE_KFILEMETADATASUPPORT=OFF \
                           -DENABLE_AKONADICONTACTSUPPORT=OFF \
                           -DENABLE_MYSQLSUPPORT=ON \
                           -DENABLE_INTERNALMYSQL=ON \
                           -DENABLE_MEDIAPLAYER=OFF \
                           -DENABLE_DBUS=OFF \
                           -DENABLE_APPSTYLES=ON \
                           -DENABLE_QWEBENGINE=ON \
                           -DENABLE_QTMULTIMEDIA=ON \
                           ${OPTIONS} \
                           -Wno-dev

